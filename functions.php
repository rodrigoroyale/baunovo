<?php // custom functions.php template

// add feed links to header
if (function_exists('automatic_feed_links')) {
	automatic_feed_links();
} else {
	return;
}


// smart jquery inclusion
if (!is_admin()) {
	wp_deregister_script('jquery');
	wp_register_script('jquery', ( get_template_directory_uri() . '/assets/js/jquery.min.js'), false, '1.3.2');
	wp_enqueue_script('jquery');
}


function mix($file) {
  static $manifest = null;

  if (is_null($manifest)) {
    $url = get_template_directory() . '/dist/mix-manifest.json';
      $manifest = json_decode(file_get_contents($url ), true);
  }

  if (isset($manifest[$file])) {
      return '/'.$manifest[$file];
  }


  // trigger_error(sprintf('mix-manifest.json file does not exists', $file), E_USER_ERROR);
}

//Include Scripts and Styles
function scripts_styles_mix() {
	// wp_enqueue_script( 'manifest', get_template_directory_uri() . '/dist' . mix('/js/manifest.js'), [], true );
	// wp_enqueue_script( 'jenndee_vendor_js', get_template_directory_uri() . '/dist' . mix('/js/vendor.js'), [], true );
	// wp_enqueue_style( 'jenndee_vendor_css', get_template_directory_uri() . '/dist' . mix('/css/vendor.css'), false );
	wp_enqueue_script( 'jenndee_js', get_template_directory_uri() . '/dist' . mix('/js/scripts.js'), [], true );
  wp_enqueue_style( 'styles', get_template_directory_uri() . '/dist' . mix('/css/styles.css'), false );
}
add_action( 'wp_enqueue_scripts', 'scripts_styles_mix' );

//google maps api key
function my_acf_init() {
	
	acf_update_setting('google_api_key', 'AIzaSyAeRbWJH5Ce-oleT9Yr_sKIg9qGz4WU1qE');
}

add_action('acf/init', 'my_acf_init');

// enable threaded comments
function enable_threaded_comments(){
	if (!is_admin()) {
		if (is_singular() AND comments_open() AND (get_option('thread_comments') == 1))
			wp_enqueue_script('comment-reply');
		}
}
add_action('get_header', 'enable_threaded_comments');


// remove junk from head
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'feed_links', 2);
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'start_post_rel_link', 10, 0);
remove_action('wp_head', 'parent_post_rel_link', 10, 0);
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0);

// custom excerpt length
function custom_excerpt_length($length) {
	return 20;
}
add_filter('excerpt_length', 'custom_excerpt_length');


// custom excerpt ellipses for 2.9+
function custom_excerpt_more($more) {
	return '...';
}
add_filter('excerpt_more', 'custom_excerpt_more');

/* custom excerpt ellipses for 2.8-
function custom_excerpt_more($excerpt) {
	return str_replace('[...]', '...', $excerpt);
}
add_filter('wp_trim_excerpt', 'custom_excerpt_more'); 
*/


// no more jumping for read more link
function no_more_jumping($post) {
	return '<a href="'.get_permalink($post->ID).'" class="read-more">'.'Continue Reading'.'</a>';
}
add_filter('excerpt_more', 'no_more_jumping');


// add a favicon to your 
function blog_favicon() {
	echo '<link rel="Shortcut Icon" type="image/x-icon" href="'.get_bloginfo('wpurl').'/favicon.ico" />';
}
add_action('wp_head', 'blog_favicon');


// // add a favicon for your admin
// function admin_favicon() {
// 	echo '<link rel="Shortcut Icon" type="image/x-icon" href="'.get_bloginfo('stylesheet_directory').'/images/favicon.png" />';
// }
// add_action('admin_head', 'admin_favicon');


// // custom admin login logo
// function custom_login_logo() {
// 	echo '<style type="text/css">
// 	h1 a { background-image: url('.get_bloginfo('template_directory').'/images/custom-login-logo.png) !important; }
// 	</style>';
// }
// add_action('login_head', 'custom_login_logo');


// disable all widget areas
function disable_all_widgets($sidebars_widgets) {
	//if (is_home())
		$sidebars_widgets = array(false);
	return $sidebars_widgets;
}
add_filter('sidebars_widgets', 'disable_all_widgets');


// kill the admin nag
if (!current_user_can('edit_users')) {
	add_action('init', create_function('$a', "remove_action('init', 'wp_version_check');"), 2);
	add_filter('pre_option_update_core', create_function('$a', "return null;"));
}


// category id in body and post class
function category_id_class($classes) {
	global $post;
	foreach((get_the_category($post->ID)) as $category)
		$classes [] = 'cat-' . $category->cat_ID . '-id';
		return $classes;
}
add_filter('post_class', 'category_id_class');
add_filter('body_class', 'category_id_class');


// get the first category id
function get_first_category_ID() {
	$category = get_the_category();
	return $category[0]->cat_ID;
}

// Page Thumbnails
add_theme_support( 'post-thumbnails' );


//Page Slug Body Class
function add_slug_body_class( $classes ) {
global $post;
if ( isset( $post ) ) {
$classes[] = $post->post_type . '-' . $post->post_name;
}
return $classes;
}
add_filter( 'body_class', 'add_slug_body_class' );


// custom menus
function register_my_menus() {
  register_nav_menus(
    array(
      'nav-main' => __( 'Main Nav' ),
      'nav-gastro' => __( 'Gastro Nav' ),
      'nav-moda' => __( 'Moda Nav' ),
      'nav-blog' => __( 'Blog Nav' ),
      'nav-social' => __( 'Social Nav' ),
      'nav-country' => __( 'Country Nav' ),
    )
  );
}

add_action( 'init', 'register_my_menus' );

// woocommerce support
add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
  add_theme_support( 'woocommerce' );
}

/**
 * include Woocommerce Overrides
 */
get_template_part('includes/woocommerce/overrides');