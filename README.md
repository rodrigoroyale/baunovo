# README #

### What is this repository for? ###

Basic wordpress theme for creating websites in Royale Studios.
Version 1.0

### How do I get set up? ###

This wordpress theme uses webpack and laravel mix to run.

The task runner optimices scss, js and vendors.

To install, pull the repository in wp-content/themes/ and run:

```
npm install

```

BrowserSync port and proxy needs to be updated in order to run correctly.

```
npm run build

npm run watch
```

### Webpack Mix ###

```
mix.setPublicPath('dist');
mix.setResourceRoot('../');

mix.js('src/js/scripts.js', 'dist/js')
   .sourceMaps()
   .setPublicPath('dist')
   .version();

mix.sass('src/scss/styles.scss', 'dist/css')
   .sourceMaps()
   .setPublicPath('dist')
   .version();

mix.browserSync({
    port: '3120',
    proxy: 'localhost:8120',
    files: [
      'dist/**/*',
      '*.php',
      '**/*.php'
    ]
})

mix.copy('src/img/**/*', 'dist/img')
   .copy('src/fonts/**/*', 'dist/fonts/')

```

### Who do I talk to? ###

If you need more information about this Repository, contact: Javier Castillo at javier@royalestudios.com