(function($) {

	$(document).ready(function() { 

		//Trigger Main Navigation

			var mainNavTrigger = document.querySelector('.cjc-nav-trigger');
	    if(mainNavTrigger){
	      mainNavTrigger.addEventListener('click', function(){
	        this.classList.toggle('open');
	        document.querySelector('.menu-main-nav-container').classList.toggle('visible');
	        document.querySelector('.cjc-header').classList.toggle('nav-open');
	        document.querySelector("html").classList.toggle('overflow-hidden');
	      })
	    }

	  //Scroll logo switch
		  window.onscroll = function(){
		    var windowHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);

		    if (window.pageYOffset > 0){
		      document.querySelector('.cjc-header').classList.add('scrolling');
		    } else {
		      document.querySelector('.cjc-header').classList.remove('scrolling');
		    }
		  }

		//Custom Input Number

	    $('.product-quantity').each(function() {
	      var quantity = $(this).find('.quantity'),
	        input = quantity.find('input[type="number"]'),
	        min = input.attr('min'),
	        max = input.attr('max');
	        
	      var quantityNav = $(this).find('.quantity-nav')
	        btnUp = quantityNav.find('.quantity-up'),
	        btnDown = quantityNav.find('.quantity-down'),

	      btnUp.click(function() {
	        var oldValue = parseFloat(input.val());
	        
	        var newVal = oldValue + 1;
	        
	        quantity.find("input").val(newVal);
	        quantity.find("input").trigger("change");

	        console.log('going up outside ajaxComplete')
	      });

	      btnDown.click(function() {
	        var oldValue = parseFloat(input.val());
	        if (oldValue <= min) {
	          var newVal = oldValue;
	        } else {
	          var newVal = oldValue - 1;
	        }
	        quantity.find("input").val(newVal);
	        quantity.find("input").trigger("change");

	        console.log('going-down outside ajaxComplete')
	      });
	    });


      $( document ).ajaxComplete(function() {
			  $('.product-quantity').each(function() {
		      var quantity = $(this).find('.quantity'),
		        input = quantity.find('input[type="number"]'),
		        min = input.attr('min'),
		        max = input.attr('max');
		        
		      var quantityNav = $(this).find('.quantity-nav')
		        btnUp = quantityNav.find('.quantity-up'),
		        btnDown = quantityNav.find('.quantity-down'),

		      btnUp.click(function() {
		        var oldValue = parseFloat(input.val());
		        
		        var newVal = oldValue + 1;
		        
		        quantity.find("input").val(newVal);
		        quantity.find("input").trigger("change");

		        console.log('going up')
		      });

		      btnDown.click(function() {
		        var oldValue = parseFloat(input.val());
		        if (oldValue <= min) {
		          var newVal = oldValue;
		        } else {
		          var newVal = oldValue - 1;
		        }
		        quantity.find("input").val(newVal);
		        quantity.find("input").trigger("change");

		        console.log('going-down')
		      });
		    });  
			});
	  
	  //Swipers

	  	//#Featured Products Slider

				var mySwiper = new Swiper ('.swiper-container', {
			    // Optional parameters
			    slidesPerView: 1,
			    centeredSlides: true,
			    direction: 'horizontal',
			    loop: true,
			    autoplay: {
		        delay: 5000,
		        disableOnInteraction: false,
		      },

			    // If we need pagination
			    pagination: {
		        el: '.swiper-pagination',
		        clickable: true,
		        renderBullet: function (index, className) {
		          return '<span class="' + className + '">' + (index + 1) + '</span>';
		        },
			    },

			    // Navigation arrows
			    navigation: {
			      nextEl: '.swiper-button-next',
			      prevEl: '.swiper-button-prev',
			    }

			  })


	});

})(jQuery);