<?php
/**
 * Archive Template
 **/
?>

<?php get_header(); ?>

  <?php if ( have_posts() ) : ?>

     
    <!--  Start the Loop  -->
    <?php while ( have_posts() ) : the_post(); ?> 

      <h2><?php the_title(); ?></h2>
      <?php the_excerpt(); ?>

    <?php endwhile; ?>

    <?php the_posts_navigation(); ?>

    <?php else: ?>

      <h1>No post found</h1>

    <?php endif;  ?>



<?php get_footer(); ?>
