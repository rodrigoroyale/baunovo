<?php
/**
 * Single Template
 */
?>
<?php get_header(); ?>


<?php while(have_posts()) : the_post(); ?>


  <?php the_category(); ?>

  <?php if ( has_post_thumbnail() ): ?>
    <img src="<?php the_post_thumbnail_url( "large" ); ?>" width="100%" alt="<?php the_title(); ?>">
  <?php else: endif; ?>
	
	<h1><?php the_title(); ?></h1>
	<p><?php the_time(get_option('date_format')); ?> - By <?php the_author() ?>
	  
  <?php the_content(); ?>
  
<?php endwhile; ?>


<!-- Articulos Relacionados -->
  <div>
    
    <h2>Últimas Publicaciones</h2>
      
    <?php
    $pid = get_the_id();
    $categories = get_the_category($pid);

    $cat_title = '';
    $cat_link = '';
    
    if ($categories) {

    foreach ($categories as $key => $cat) {

    if ($key === 0){
      $cat_title = $cat->name;
      $cat_link = get_term_link($cat);

      $args=array(
      'category__in' => array($cat->term_id),
      'post__not_in' => array($pid),
      'cat' => '-194',
      'posts_per_page' => 3
      );
      $my_query = new WP_Query($args);
      if( $my_query->have_posts() ) {
      while ($my_query->have_posts()) : $my_query->the_post(); ?>

      <div class="saul-post-card">
        <a href="<?php the_permalink(); ?>"><img class="saul-post-card__image" src="<?php the_post_thumbnail_url( "medium" ); ?>"></a>
        <a href="<?php the_permalink(); ?>"><h3 class="saul-post-card__title"><?php the_title() ?></h3></a>
        <?php the_excerpt() ?>
      </div>

    <?php endwhile; } wp_reset_query(); } } }?>

    <a href="<?php echo $cat_link ?>" class="saul-btn red">Ver más sobre <?= $cat_title; ?></a>

  </div>

<?php get_footer(); ?>