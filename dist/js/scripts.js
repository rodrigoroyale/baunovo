/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/js/scripts.js":
/***/ (function(module, exports) {

(function ($) {

	$(document).ready(function () {

		//Trigger Main Navigation

		var mainNavTrigger = document.querySelector('.cjc-nav-trigger');
		if (mainNavTrigger) {
			mainNavTrigger.addEventListener('click', function () {
				this.classList.toggle('open');
				document.querySelector('.menu-main-nav-container').classList.toggle('visible');
				document.querySelector('.cjc-header').classList.toggle('nav-open');
				document.querySelector("html").classList.toggle('overflow-hidden');
			});
		}

		//Scroll logo switch
		window.onscroll = function () {
			var windowHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);

			if (window.pageYOffset > 0) {
				document.querySelector('.cjc-header').classList.add('scrolling');
			} else {
				document.querySelector('.cjc-header').classList.remove('scrolling');
			}
		};

		//Custom Input Number

		$('.product-quantity').each(function () {
			var quantity = $(this).find('.quantity'),
			    input = quantity.find('input[type="number"]'),
			    min = input.attr('min'),
			    max = input.attr('max');

			var quantityNav = $(this).find('.quantity-nav');
			btnUp = quantityNav.find('.quantity-up'), btnDown = quantityNav.find('.quantity-down'), btnUp.click(function () {
				var oldValue = parseFloat(input.val());

				var newVal = oldValue + 1;

				quantity.find("input").val(newVal);
				quantity.find("input").trigger("change");

				console.log('going up outside ajaxComplete');
			});

			btnDown.click(function () {
				var oldValue = parseFloat(input.val());
				if (oldValue <= min) {
					var newVal = oldValue;
				} else {
					var newVal = oldValue - 1;
				}
				quantity.find("input").val(newVal);
				quantity.find("input").trigger("change");

				console.log('going-down outside ajaxComplete');
			});
		});

		$(document).ajaxComplete(function () {
			$('.product-quantity').each(function () {
				var quantity = $(this).find('.quantity'),
				    input = quantity.find('input[type="number"]'),
				    min = input.attr('min'),
				    max = input.attr('max');

				var quantityNav = $(this).find('.quantity-nav');
				btnUp = quantityNav.find('.quantity-up'), btnDown = quantityNav.find('.quantity-down'), btnUp.click(function () {
					var oldValue = parseFloat(input.val());

					var newVal = oldValue + 1;

					quantity.find("input").val(newVal);
					quantity.find("input").trigger("change");

					console.log('going up');
				});

				btnDown.click(function () {
					var oldValue = parseFloat(input.val());
					if (oldValue <= min) {
						var newVal = oldValue;
					} else {
						var newVal = oldValue - 1;
					}
					quantity.find("input").val(newVal);
					quantity.find("input").trigger("change");

					console.log('going-down');
				});
			});
		});

		//Swipers

		//#Featured Products Slider

		var mySwiper = new Swiper('.swiper-container', {
			// Optional parameters
			slidesPerView: 1,
			centeredSlides: true,
			direction: 'horizontal',
			loop: true,
			autoplay: {
				delay: 5000,
				disableOnInteraction: false
			},

			// If we need pagination
			pagination: {
				el: '.swiper-pagination',
				clickable: true,
				renderBullet: function renderBullet(index, className) {
					return '<span class="' + className + '">' + (index + 1) + '</span>';
				}
			},

			// Navigation arrows
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev'
			}

		});
	});
})(jQuery);

/***/ }),

/***/ "./src/scss/styles.scss":
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__("./src/js/scripts.js");
module.exports = __webpack_require__("./src/scss/styles.scss");


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgNTQxMDc5ZGY3MmY1ZDNhNjkzY2IiLCJ3ZWJwYWNrOi8vLy4vc3JjL2pzL3NjcmlwdHMuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3Njc3Mvc3R5bGVzLnNjc3M/OWYyMCJdLCJuYW1lcyI6WyIkIiwiZG9jdW1lbnQiLCJyZWFkeSIsIm1haW5OYXZUcmlnZ2VyIiwicXVlcnlTZWxlY3RvciIsImFkZEV2ZW50TGlzdGVuZXIiLCJjbGFzc0xpc3QiLCJ0b2dnbGUiLCJ3aW5kb3ciLCJvbnNjcm9sbCIsIndpbmRvd0hlaWdodCIsIk1hdGgiLCJtYXgiLCJkb2N1bWVudEVsZW1lbnQiLCJjbGllbnRIZWlnaHQiLCJpbm5lckhlaWdodCIsInBhZ2VZT2Zmc2V0IiwiYWRkIiwicmVtb3ZlIiwiZWFjaCIsInF1YW50aXR5IiwiZmluZCIsImlucHV0IiwibWluIiwiYXR0ciIsInF1YW50aXR5TmF2IiwiYnRuVXAiLCJidG5Eb3duIiwiY2xpY2siLCJvbGRWYWx1ZSIsInBhcnNlRmxvYXQiLCJ2YWwiLCJuZXdWYWwiLCJ0cmlnZ2VyIiwiY29uc29sZSIsImxvZyIsImFqYXhDb21wbGV0ZSIsIm15U3dpcGVyIiwiU3dpcGVyIiwic2xpZGVzUGVyVmlldyIsImNlbnRlcmVkU2xpZGVzIiwiZGlyZWN0aW9uIiwibG9vcCIsImF1dG9wbGF5IiwiZGVsYXkiLCJkaXNhYmxlT25JbnRlcmFjdGlvbiIsInBhZ2luYXRpb24iLCJlbCIsImNsaWNrYWJsZSIsInJlbmRlckJ1bGxldCIsImluZGV4IiwiY2xhc3NOYW1lIiwibmF2aWdhdGlvbiIsIm5leHRFbCIsInByZXZFbCIsImpRdWVyeSJdLCJtYXBwaW5ncyI6IjtBQUFBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOzs7QUFHQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBMkIsMEJBQTBCLEVBQUU7QUFDdkQseUNBQWlDLGVBQWU7QUFDaEQ7QUFDQTtBQUNBOztBQUVBO0FBQ0EsOERBQXNELCtEQUErRDs7QUFFckg7QUFDQTs7QUFFQTtBQUNBOzs7Ozs7OztBQzdEQSxDQUFDLFVBQVNBLENBQVQsRUFBWTs7QUFFWkEsR0FBRUMsUUFBRixFQUFZQyxLQUFaLENBQWtCLFlBQVc7O0FBRTVCOztBQUVDLE1BQUlDLGlCQUFpQkYsU0FBU0csYUFBVCxDQUF1QixrQkFBdkIsQ0FBckI7QUFDRSxNQUFHRCxjQUFILEVBQWtCO0FBQ2hCQSxrQkFBZUUsZ0JBQWYsQ0FBZ0MsT0FBaEMsRUFBeUMsWUFBVTtBQUNqRCxTQUFLQyxTQUFMLENBQWVDLE1BQWYsQ0FBc0IsTUFBdEI7QUFDQU4sYUFBU0csYUFBVCxDQUF1QiwwQkFBdkIsRUFBbURFLFNBQW5ELENBQTZEQyxNQUE3RCxDQUFvRSxTQUFwRTtBQUNBTixhQUFTRyxhQUFULENBQXVCLGFBQXZCLEVBQXNDRSxTQUF0QyxDQUFnREMsTUFBaEQsQ0FBdUQsVUFBdkQ7QUFDQU4sYUFBU0csYUFBVCxDQUF1QixNQUF2QixFQUErQkUsU0FBL0IsQ0FBeUNDLE1BQXpDLENBQWdELGlCQUFoRDtBQUNELElBTEQ7QUFNRDs7QUFFSDtBQUNDQyxTQUFPQyxRQUFQLEdBQWtCLFlBQVU7QUFDMUIsT0FBSUMsZUFBZUMsS0FBS0MsR0FBTCxDQUFTWCxTQUFTWSxlQUFULENBQXlCQyxZQUFsQyxFQUFnRE4sT0FBT08sV0FBUCxJQUFzQixDQUF0RSxDQUFuQjs7QUFFQSxPQUFJUCxPQUFPUSxXQUFQLEdBQXFCLENBQXpCLEVBQTJCO0FBQ3pCZixhQUFTRyxhQUFULENBQXVCLGFBQXZCLEVBQXNDRSxTQUF0QyxDQUFnRFcsR0FBaEQsQ0FBb0QsV0FBcEQ7QUFDRCxJQUZELE1BRU87QUFDTGhCLGFBQVNHLGFBQVQsQ0FBdUIsYUFBdkIsRUFBc0NFLFNBQXRDLENBQWdEWSxNQUFoRCxDQUF1RCxXQUF2RDtBQUNEO0FBQ0YsR0FSRDs7QUFVRjs7QUFFR2xCLElBQUUsbUJBQUYsRUFBdUJtQixJQUF2QixDQUE0QixZQUFXO0FBQ3JDLE9BQUlDLFdBQVdwQixFQUFFLElBQUYsRUFBUXFCLElBQVIsQ0FBYSxXQUFiLENBQWY7QUFBQSxPQUNFQyxRQUFRRixTQUFTQyxJQUFULENBQWMsc0JBQWQsQ0FEVjtBQUFBLE9BRUVFLE1BQU1ELE1BQU1FLElBQU4sQ0FBVyxLQUFYLENBRlI7QUFBQSxPQUdFWixNQUFNVSxNQUFNRSxJQUFOLENBQVcsS0FBWCxDQUhSOztBQUtBLE9BQUlDLGNBQWN6QixFQUFFLElBQUYsRUFBUXFCLElBQVIsQ0FBYSxlQUFiLENBQWxCO0FBQ0VLLFdBQVFELFlBQVlKLElBQVosQ0FBaUIsY0FBakIsQ0FBUixFQUNBTSxVQUFVRixZQUFZSixJQUFaLENBQWlCLGdCQUFqQixDQURWLEVBR0ZLLE1BQU1FLEtBQU4sQ0FBWSxZQUFXO0FBQ3JCLFFBQUlDLFdBQVdDLFdBQVdSLE1BQU1TLEdBQU4sRUFBWCxDQUFmOztBQUVBLFFBQUlDLFNBQVNILFdBQVcsQ0FBeEI7O0FBRUFULGFBQVNDLElBQVQsQ0FBYyxPQUFkLEVBQXVCVSxHQUF2QixDQUEyQkMsTUFBM0I7QUFDQVosYUFBU0MsSUFBVCxDQUFjLE9BQWQsRUFBdUJZLE9BQXZCLENBQStCLFFBQS9COztBQUVBQyxZQUFRQyxHQUFSLENBQVksK0JBQVo7QUFDRCxJQVRELENBSEU7O0FBY0ZSLFdBQVFDLEtBQVIsQ0FBYyxZQUFXO0FBQ3ZCLFFBQUlDLFdBQVdDLFdBQVdSLE1BQU1TLEdBQU4sRUFBWCxDQUFmO0FBQ0EsUUFBSUYsWUFBWU4sR0FBaEIsRUFBcUI7QUFDbkIsU0FBSVMsU0FBU0gsUUFBYjtBQUNELEtBRkQsTUFFTztBQUNMLFNBQUlHLFNBQVNILFdBQVcsQ0FBeEI7QUFDRDtBQUNEVCxhQUFTQyxJQUFULENBQWMsT0FBZCxFQUF1QlUsR0FBdkIsQ0FBMkJDLE1BQTNCO0FBQ0FaLGFBQVNDLElBQVQsQ0FBYyxPQUFkLEVBQXVCWSxPQUF2QixDQUErQixRQUEvQjs7QUFFQUMsWUFBUUMsR0FBUixDQUFZLGlDQUFaO0FBQ0QsSUFYRDtBQVlELEdBakNEOztBQW9DQ25DLElBQUdDLFFBQUgsRUFBY21DLFlBQWQsQ0FBMkIsWUFBVztBQUN2Q3BDLEtBQUUsbUJBQUYsRUFBdUJtQixJQUF2QixDQUE0QixZQUFXO0FBQ3BDLFFBQUlDLFdBQVdwQixFQUFFLElBQUYsRUFBUXFCLElBQVIsQ0FBYSxXQUFiLENBQWY7QUFBQSxRQUNFQyxRQUFRRixTQUFTQyxJQUFULENBQWMsc0JBQWQsQ0FEVjtBQUFBLFFBRUVFLE1BQU1ELE1BQU1FLElBQU4sQ0FBVyxLQUFYLENBRlI7QUFBQSxRQUdFWixNQUFNVSxNQUFNRSxJQUFOLENBQVcsS0FBWCxDQUhSOztBQUtBLFFBQUlDLGNBQWN6QixFQUFFLElBQUYsRUFBUXFCLElBQVIsQ0FBYSxlQUFiLENBQWxCO0FBQ0VLLFlBQVFELFlBQVlKLElBQVosQ0FBaUIsY0FBakIsQ0FBUixFQUNBTSxVQUFVRixZQUFZSixJQUFaLENBQWlCLGdCQUFqQixDQURWLEVBR0ZLLE1BQU1FLEtBQU4sQ0FBWSxZQUFXO0FBQ3JCLFNBQUlDLFdBQVdDLFdBQVdSLE1BQU1TLEdBQU4sRUFBWCxDQUFmOztBQUVBLFNBQUlDLFNBQVNILFdBQVcsQ0FBeEI7O0FBRUFULGNBQVNDLElBQVQsQ0FBYyxPQUFkLEVBQXVCVSxHQUF2QixDQUEyQkMsTUFBM0I7QUFDQVosY0FBU0MsSUFBVCxDQUFjLE9BQWQsRUFBdUJZLE9BQXZCLENBQStCLFFBQS9COztBQUVBQyxhQUFRQyxHQUFSLENBQVksVUFBWjtBQUNELEtBVEQsQ0FIRTs7QUFjRlIsWUFBUUMsS0FBUixDQUFjLFlBQVc7QUFDdkIsU0FBSUMsV0FBV0MsV0FBV1IsTUFBTVMsR0FBTixFQUFYLENBQWY7QUFDQSxTQUFJRixZQUFZTixHQUFoQixFQUFxQjtBQUNuQixVQUFJUyxTQUFTSCxRQUFiO0FBQ0QsTUFGRCxNQUVPO0FBQ0wsVUFBSUcsU0FBU0gsV0FBVyxDQUF4QjtBQUNEO0FBQ0RULGNBQVNDLElBQVQsQ0FBYyxPQUFkLEVBQXVCVSxHQUF2QixDQUEyQkMsTUFBM0I7QUFDQVosY0FBU0MsSUFBVCxDQUFjLE9BQWQsRUFBdUJZLE9BQXZCLENBQStCLFFBQS9COztBQUVBQyxhQUFRQyxHQUFSLENBQVksWUFBWjtBQUNELEtBWEQ7QUFZRCxJQWpDRjtBQWtDRCxHQW5DRTs7QUFxQ0g7O0FBRUM7O0FBRUEsTUFBSUUsV0FBVyxJQUFJQyxNQUFKLENBQVksbUJBQVosRUFBaUM7QUFDN0M7QUFDQUMsa0JBQWUsQ0FGOEI7QUFHN0NDLG1CQUFnQixJQUg2QjtBQUk3Q0MsY0FBVyxZQUprQztBQUs3Q0MsU0FBTSxJQUx1QztBQU03Q0MsYUFBVTtBQUNQQyxXQUFPLElBREE7QUFFUEMsMEJBQXNCO0FBRmYsSUFObUM7O0FBVzdDO0FBQ0FDLGVBQVk7QUFDVEMsUUFBSSxvQkFESztBQUVUQyxlQUFXLElBRkY7QUFHVEMsa0JBQWMsc0JBQVVDLEtBQVYsRUFBaUJDLFNBQWpCLEVBQTRCO0FBQ3hDLFlBQU8sa0JBQWtCQSxTQUFsQixHQUE4QixJQUE5QixJQUFzQ0QsUUFBUSxDQUE5QyxJQUFtRCxTQUExRDtBQUNEO0FBTFEsSUFaaUM7O0FBb0I3QztBQUNBRSxlQUFZO0FBQ1ZDLFlBQVEscUJBREU7QUFFVkMsWUFBUTtBQUZFOztBQXJCaUMsR0FBakMsQ0FBZjtBQTZCRixFQXJJRDtBQXVJQSxDQXpJRCxFQXlJR0MsTUF6SUgsRTs7Ozs7OztBQ0FBLHlDIiwiZmlsZSI6Ii9qcy9zY3JpcHRzLmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7XG4gXHRcdFx0XHRjb25maWd1cmFibGU6IGZhbHNlLFxuIFx0XHRcdFx0ZW51bWVyYWJsZTogdHJ1ZSxcbiBcdFx0XHRcdGdldDogZ2V0dGVyXG4gXHRcdFx0fSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gMCk7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gd2VicGFjay9ib290c3RyYXAgNTQxMDc5ZGY3MmY1ZDNhNjkzY2IiLCIoZnVuY3Rpb24oJCkge1xuXG5cdCQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uKCkgeyBcblxuXHRcdC8vVHJpZ2dlciBNYWluIE5hdmlnYXRpb25cblxuXHRcdFx0dmFyIG1haW5OYXZUcmlnZ2VyID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLmNqYy1uYXYtdHJpZ2dlcicpO1xuXHQgICAgaWYobWFpbk5hdlRyaWdnZXIpe1xuXHQgICAgICBtYWluTmF2VHJpZ2dlci5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIGZ1bmN0aW9uKCl7XG5cdCAgICAgICAgdGhpcy5jbGFzc0xpc3QudG9nZ2xlKCdvcGVuJyk7XG5cdCAgICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLm1lbnUtbWFpbi1uYXYtY29udGFpbmVyJykuY2xhc3NMaXN0LnRvZ2dsZSgndmlzaWJsZScpO1xuXHQgICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5jamMtaGVhZGVyJykuY2xhc3NMaXN0LnRvZ2dsZSgnbmF2LW9wZW4nKTtcblx0ICAgICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiaHRtbFwiKS5jbGFzc0xpc3QudG9nZ2xlKCdvdmVyZmxvdy1oaWRkZW4nKTtcblx0ICAgICAgfSlcblx0ICAgIH1cblxuXHQgIC8vU2Nyb2xsIGxvZ28gc3dpdGNoXG5cdFx0ICB3aW5kb3cub25zY3JvbGwgPSBmdW5jdGlvbigpe1xuXHRcdCAgICB2YXIgd2luZG93SGVpZ2h0ID0gTWF0aC5tYXgoZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LmNsaWVudEhlaWdodCwgd2luZG93LmlubmVySGVpZ2h0IHx8IDApO1xuXG5cdFx0ICAgIGlmICh3aW5kb3cucGFnZVlPZmZzZXQgPiAwKXtcblx0XHQgICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcuY2pjLWhlYWRlcicpLmNsYXNzTGlzdC5hZGQoJ3Njcm9sbGluZycpO1xuXHRcdCAgICB9IGVsc2Uge1xuXHRcdCAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5jamMtaGVhZGVyJykuY2xhc3NMaXN0LnJlbW92ZSgnc2Nyb2xsaW5nJyk7XG5cdFx0ICAgIH1cblx0XHQgIH1cblxuXHRcdC8vQ3VzdG9tIElucHV0IE51bWJlclxuXG5cdCAgICAkKCcucHJvZHVjdC1xdWFudGl0eScpLmVhY2goZnVuY3Rpb24oKSB7XG5cdCAgICAgIHZhciBxdWFudGl0eSA9ICQodGhpcykuZmluZCgnLnF1YW50aXR5JyksXG5cdCAgICAgICAgaW5wdXQgPSBxdWFudGl0eS5maW5kKCdpbnB1dFt0eXBlPVwibnVtYmVyXCJdJyksXG5cdCAgICAgICAgbWluID0gaW5wdXQuYXR0cignbWluJyksXG5cdCAgICAgICAgbWF4ID0gaW5wdXQuYXR0cignbWF4Jyk7XG5cdCAgICAgICAgXG5cdCAgICAgIHZhciBxdWFudGl0eU5hdiA9ICQodGhpcykuZmluZCgnLnF1YW50aXR5LW5hdicpXG5cdCAgICAgICAgYnRuVXAgPSBxdWFudGl0eU5hdi5maW5kKCcucXVhbnRpdHktdXAnKSxcblx0ICAgICAgICBidG5Eb3duID0gcXVhbnRpdHlOYXYuZmluZCgnLnF1YW50aXR5LWRvd24nKSxcblxuXHQgICAgICBidG5VcC5jbGljayhmdW5jdGlvbigpIHtcblx0ICAgICAgICB2YXIgb2xkVmFsdWUgPSBwYXJzZUZsb2F0KGlucHV0LnZhbCgpKTtcblx0ICAgICAgICBcblx0ICAgICAgICB2YXIgbmV3VmFsID0gb2xkVmFsdWUgKyAxO1xuXHQgICAgICAgIFxuXHQgICAgICAgIHF1YW50aXR5LmZpbmQoXCJpbnB1dFwiKS52YWwobmV3VmFsKTtcblx0ICAgICAgICBxdWFudGl0eS5maW5kKFwiaW5wdXRcIikudHJpZ2dlcihcImNoYW5nZVwiKTtcblxuXHQgICAgICAgIGNvbnNvbGUubG9nKCdnb2luZyB1cCBvdXRzaWRlIGFqYXhDb21wbGV0ZScpXG5cdCAgICAgIH0pO1xuXG5cdCAgICAgIGJ0bkRvd24uY2xpY2soZnVuY3Rpb24oKSB7XG5cdCAgICAgICAgdmFyIG9sZFZhbHVlID0gcGFyc2VGbG9hdChpbnB1dC52YWwoKSk7XG5cdCAgICAgICAgaWYgKG9sZFZhbHVlIDw9IG1pbikge1xuXHQgICAgICAgICAgdmFyIG5ld1ZhbCA9IG9sZFZhbHVlO1xuXHQgICAgICAgIH0gZWxzZSB7XG5cdCAgICAgICAgICB2YXIgbmV3VmFsID0gb2xkVmFsdWUgLSAxO1xuXHQgICAgICAgIH1cblx0ICAgICAgICBxdWFudGl0eS5maW5kKFwiaW5wdXRcIikudmFsKG5ld1ZhbCk7XG5cdCAgICAgICAgcXVhbnRpdHkuZmluZChcImlucHV0XCIpLnRyaWdnZXIoXCJjaGFuZ2VcIik7XG5cblx0ICAgICAgICBjb25zb2xlLmxvZygnZ29pbmctZG93biBvdXRzaWRlIGFqYXhDb21wbGV0ZScpXG5cdCAgICAgIH0pO1xuXHQgICAgfSk7XG5cblxuICAgICAgJCggZG9jdW1lbnQgKS5hamF4Q29tcGxldGUoZnVuY3Rpb24oKSB7XG5cdFx0XHQgICQoJy5wcm9kdWN0LXF1YW50aXR5JykuZWFjaChmdW5jdGlvbigpIHtcblx0XHQgICAgICB2YXIgcXVhbnRpdHkgPSAkKHRoaXMpLmZpbmQoJy5xdWFudGl0eScpLFxuXHRcdCAgICAgICAgaW5wdXQgPSBxdWFudGl0eS5maW5kKCdpbnB1dFt0eXBlPVwibnVtYmVyXCJdJyksXG5cdFx0ICAgICAgICBtaW4gPSBpbnB1dC5hdHRyKCdtaW4nKSxcblx0XHQgICAgICAgIG1heCA9IGlucHV0LmF0dHIoJ21heCcpO1xuXHRcdCAgICAgICAgXG5cdFx0ICAgICAgdmFyIHF1YW50aXR5TmF2ID0gJCh0aGlzKS5maW5kKCcucXVhbnRpdHktbmF2Jylcblx0XHQgICAgICAgIGJ0blVwID0gcXVhbnRpdHlOYXYuZmluZCgnLnF1YW50aXR5LXVwJyksXG5cdFx0ICAgICAgICBidG5Eb3duID0gcXVhbnRpdHlOYXYuZmluZCgnLnF1YW50aXR5LWRvd24nKSxcblxuXHRcdCAgICAgIGJ0blVwLmNsaWNrKGZ1bmN0aW9uKCkge1xuXHRcdCAgICAgICAgdmFyIG9sZFZhbHVlID0gcGFyc2VGbG9hdChpbnB1dC52YWwoKSk7XG5cdFx0ICAgICAgICBcblx0XHQgICAgICAgIHZhciBuZXdWYWwgPSBvbGRWYWx1ZSArIDE7XG5cdFx0ICAgICAgICBcblx0XHQgICAgICAgIHF1YW50aXR5LmZpbmQoXCJpbnB1dFwiKS52YWwobmV3VmFsKTtcblx0XHQgICAgICAgIHF1YW50aXR5LmZpbmQoXCJpbnB1dFwiKS50cmlnZ2VyKFwiY2hhbmdlXCIpO1xuXG5cdFx0ICAgICAgICBjb25zb2xlLmxvZygnZ29pbmcgdXAnKVxuXHRcdCAgICAgIH0pO1xuXG5cdFx0ICAgICAgYnRuRG93bi5jbGljayhmdW5jdGlvbigpIHtcblx0XHQgICAgICAgIHZhciBvbGRWYWx1ZSA9IHBhcnNlRmxvYXQoaW5wdXQudmFsKCkpO1xuXHRcdCAgICAgICAgaWYgKG9sZFZhbHVlIDw9IG1pbikge1xuXHRcdCAgICAgICAgICB2YXIgbmV3VmFsID0gb2xkVmFsdWU7XG5cdFx0ICAgICAgICB9IGVsc2Uge1xuXHRcdCAgICAgICAgICB2YXIgbmV3VmFsID0gb2xkVmFsdWUgLSAxO1xuXHRcdCAgICAgICAgfVxuXHRcdCAgICAgICAgcXVhbnRpdHkuZmluZChcImlucHV0XCIpLnZhbChuZXdWYWwpO1xuXHRcdCAgICAgICAgcXVhbnRpdHkuZmluZChcImlucHV0XCIpLnRyaWdnZXIoXCJjaGFuZ2VcIik7XG5cblx0XHQgICAgICAgIGNvbnNvbGUubG9nKCdnb2luZy1kb3duJylcblx0XHQgICAgICB9KTtcblx0XHQgICAgfSk7ICBcblx0XHRcdH0pO1xuXHQgIFxuXHQgIC8vU3dpcGVyc1xuXG5cdCAgXHQvLyNGZWF0dXJlZCBQcm9kdWN0cyBTbGlkZXJcblxuXHRcdFx0XHR2YXIgbXlTd2lwZXIgPSBuZXcgU3dpcGVyICgnLnN3aXBlci1jb250YWluZXInLCB7XG5cdFx0XHQgICAgLy8gT3B0aW9uYWwgcGFyYW1ldGVyc1xuXHRcdFx0ICAgIHNsaWRlc1BlclZpZXc6IDEsXG5cdFx0XHQgICAgY2VudGVyZWRTbGlkZXM6IHRydWUsXG5cdFx0XHQgICAgZGlyZWN0aW9uOiAnaG9yaXpvbnRhbCcsXG5cdFx0XHQgICAgbG9vcDogdHJ1ZSxcblx0XHRcdCAgICBhdXRvcGxheToge1xuXHRcdCAgICAgICAgZGVsYXk6IDUwMDAsXG5cdFx0ICAgICAgICBkaXNhYmxlT25JbnRlcmFjdGlvbjogZmFsc2UsXG5cdFx0ICAgICAgfSxcblxuXHRcdFx0ICAgIC8vIElmIHdlIG5lZWQgcGFnaW5hdGlvblxuXHRcdFx0ICAgIHBhZ2luYXRpb246IHtcblx0XHQgICAgICAgIGVsOiAnLnN3aXBlci1wYWdpbmF0aW9uJyxcblx0XHQgICAgICAgIGNsaWNrYWJsZTogdHJ1ZSxcblx0XHQgICAgICAgIHJlbmRlckJ1bGxldDogZnVuY3Rpb24gKGluZGV4LCBjbGFzc05hbWUpIHtcblx0XHQgICAgICAgICAgcmV0dXJuICc8c3BhbiBjbGFzcz1cIicgKyBjbGFzc05hbWUgKyAnXCI+JyArIChpbmRleCArIDEpICsgJzwvc3Bhbj4nO1xuXHRcdCAgICAgICAgfSxcblx0XHRcdCAgICB9LFxuXG5cdFx0XHQgICAgLy8gTmF2aWdhdGlvbiBhcnJvd3Ncblx0XHRcdCAgICBuYXZpZ2F0aW9uOiB7XG5cdFx0XHQgICAgICBuZXh0RWw6ICcuc3dpcGVyLWJ1dHRvbi1uZXh0Jyxcblx0XHRcdCAgICAgIHByZXZFbDogJy5zd2lwZXItYnV0dG9uLXByZXYnLFxuXHRcdFx0ICAgIH1cblxuXHRcdFx0ICB9KVxuXG5cblx0fSk7XG5cbn0pKGpRdWVyeSk7XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2pzL3NjcmlwdHMuanMiLCIvLyByZW1vdmVkIGJ5IGV4dHJhY3QtdGV4dC13ZWJwYWNrLXBsdWdpblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vc3JjL3Njc3Mvc3R5bGVzLnNjc3Ncbi8vIG1vZHVsZSBpZCA9IC4vc3JjL3Njc3Mvc3R5bGVzLnNjc3Ncbi8vIG1vZHVsZSBjaHVua3MgPSAwIl0sInNvdXJjZVJvb3QiOiIifQ==