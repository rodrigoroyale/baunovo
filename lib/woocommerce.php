<?php

/**
 * Adding Woocommerce Support
 */

function dida_woocommerce_support() {
    add_theme_support( 'woocommerce' );
}
add_action( 'after_setup_theme', 'dida_woocommerce_support' );

add_action( 'woocommerce_single_variation', 'woocommerce_single_variation', 10 );
add_action( 'woocommerce_single_variation', 'woocommerce_single_variation_add_to_cart_button', 20 );

/**
 * Remove woocommerce sidebar
 */
remove_action('woocommerce_sidebar', 'woocommerce_get_sidebar', 10);

remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);

function dida__theme_wrapper_start() {
  echo '<section class="dida-container no-padding" id="main"><div class="dida-content">';
}

function dida_theme_wrapper_end() {
  echo '</div></section>';
}

add_action('woocommerce_before_main_content', 'dida__theme_wrapper_start', 10);
add_action('woocommerce_after_main_content', 'dida_theme_wrapper_end', 10);

/**
 * Add woocommerce gallery
 */
add_theme_support( 'wc-product-gallery-zoom' );
add_theme_support( 'wc-product-gallery-lightbox' );
add_theme_support( 'wc-product-gallery-slider' );


// Or just remove them all in one line
add_filter( 'woocommerce_enqueue_styles', '__return_false' );

/**
 * Remove reviews tab
 */
function dida_remove_reviews_tab($tabs) {
 unset($tabs['reviews']);
 unset($tabs['description']);
 unset($tabs['additional_information']);
 return $tabs;
}
add_filter('woocommerce_product_tabs', 'dida_remove_reviews_tab', 100);

/**
 * Remove product meta
 */
//remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);

/**
 * Change product price position
 */
// remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
// add_action( 'woocommerce_single_product_summary', 'woocommerce_template_loop_price', 25 );

// Remove short description
// remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
// remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 20 );

/**
 * Remove default breadcrumbs
 */
remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );


/**
 * Remove sale flash
 */
remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash', 10 );
remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10 );

/**
 * Change product layout
 */
// remove_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10 );

function dida_open_summary_block() {
    echo '<div class="product__summary">';
}

function dida_close_summary_block()
{
    echo '</div>';
}

add_action( 'woocommerce_single_product_summary', 'dida_open_summary_block', 1 );
add_action( 'woocommerce_after_single_product_summary', 'dida_close_summary_block', 10 );

remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
remove_action( 'woocommerce_after_shop_loop', 'woocommerce_pagination', 10 );


// TODO: Quitar acces control origin para produccion

add_action( 'init', 'handle_preflight' );

function handle_preflight() {
    header("Access-Control-Allow-Origin: " . get_http_origin());
    header("Access-Control-Allow-Methods: POST, GET, OPTIONS, PUT, DELETE");
    header("Access-Control-Allow-Credentials: true");
    header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");

    if ( 'OPTIONS' == $_SERVER['REQUEST_METHOD'] ) {
        status_header(200);
        exit();
    }
}
