<?php
/**
 * The main template file for pages
 */
?>

<?php get_header(); ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

	<?php if( have_rows('components') ): ?>

		<?php while ( have_rows('components') ) : the_row(); ?>

			<?php if( get_row_layout() == 'component_one' ): ?>
				<?php get_template_part('includes/component_one'); ?>
			<?php endif; ?>

			<?php if( get_row_layout() == 'component_two' ): ?>
				<?php get_template_part('includes/component_two'); ?>
			<?php endif; ?>

		<?php endwhile; ?>

	<?php endif; ?>

<?php endwhile; else: ?>
				
	<p><?php _e('Sorry, no locations matched your criteria.'); ?></p>

<?php endif; ?>


<?php get_footer(); ?>